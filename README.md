# README #

When the "use AVMutableComposition" switch is on, `AVMutableComposition` is used for `AVAssetExportSession` and audio/video sync will be different with a few source video.

When the switch is off, `AVAsset` which is extracted from `PHImageManager.default().requestAVAsset` is used for `AVAssetExportSession` and there is no audio/video sync problem.

After set the switch as you want to set for exporting, tab a video in the collection view under the switch to start exporting. Then you can check the video exported in Photos app.
