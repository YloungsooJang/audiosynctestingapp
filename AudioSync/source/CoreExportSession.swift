//
//  CoreExportSession.swift
//  AudioSync
//
//  Created by 장영수 on 2020/10/16.
//

import AVKit
import Photos

protocol CoreExportSessionDelegate: class {
  func coreExportSessionCompletedHandler()
  func coreExportSessionProgressHandler(progress: Float)
}

class CoreExportSession {

  weak var delegate: CoreExportSessionDelegate?

  private var avAsset: AVAsset
  private var outputURL: URL = FileManager.default.temporaryDirectory.appendingPathComponent("File from AudioSync.mov")

  private var exportSession: AVAssetExportSession?
  private var timer: Timer?
  private var progress: Float = 0 {
    didSet {
      if progress != oldValue {
        delegate?.coreExportSessionProgressHandler(progress: progress)
      }
    }
  }

  private let timerInterval: TimeInterval = 0.5

  init(
    avAsset: AVAsset
  ) {
    self.avAsset = avAsset
  }

  // public

  func export() {
    // delete old exported file
    try? FileManager.default.removeItem(at: outputURL)

    let availablePresets = AVAssetExportSession.exportPresets(compatibleWith: avAsset)
    guard let exportSession = AVAssetExportSession(
      asset: avAsset,
      presetName: availablePresets.contains(AVAssetExportPresetHEVCHighestQuality)
        ? AVAssetExportPresetHEVCHighestQuality
        : AVAssetExportPresetHighestQuality
    ) else {
      return
    }
    self.exportSession = exportSession
    exportSession.outputURL = outputURL
    exportSession.outputFileType = .mov
    exportSession.shouldOptimizeForNetworkUse = true
    setTimer()

    exportSession.exportAsynchronously {
      self.exportDidFinish(session: exportSession)
    }
  }

  private func exportDidFinish(session: AVAssetExportSession) {
    invalidateTimer()
    PHPhotoLibrary.shared().performChanges({
      PHAssetChangeRequest.creationRequestForAssetFromVideo(
        atFileURL: self.outputURL
      )
    }, completionHandler: { success, error in
      if let nserror = error as NSError? {
        print(nserror.debugDescription)
      }
      self.delegate?.coreExportSessionCompletedHandler()
    })
  }

  private func setTimer() {
    self.timer = Timer.scheduledTimer(
      withTimeInterval: timerInterval,
      repeats: true
    ) { [weak self] _ in
      guard let strongSelf = self, let session = strongSelf.exportSession else { return }
      strongSelf.progress = session.progress
    }
  }

  private func invalidateTimer() {
    self.timer?.invalidate()
    self.timer = nil
  }

}
