//
//  misc.swift
//  AudioSync
//
//  Created by 장영수 on 2020/10/16.
//

import UIKit

extension UIView {

  func addSubviewByFilling(_ subview: UIView, edgeInsets: UIEdgeInsets = .zero) {
    addSubview(subview)

    subview.translatesAutoresizingMaskIntoConstraints = false
    subview.topAnchor.constraint(equalTo: topAnchor, constant: edgeInsets.top).isActive = true
    subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -edgeInsets.bottom).isActive = true
    subview.leftAnchor.constraint(equalTo: leftAnchor, constant: edgeInsets.left).isActive = true
    subview.rightAnchor.constraint(equalTo: rightAnchor, constant: -edgeInsets.right).isActive = true
  }

  func addSubviewByFilling(_ subview: UIView, padding: CGFloat) {
    addSubview(subview)

    subview.translatesAutoresizingMaskIntoConstraints = false
    subview.topAnchor.constraint(equalTo: topAnchor, constant: padding).isActive = true
    subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding).isActive = true
    subview.leftAnchor.constraint(equalTo: leftAnchor, constant: padding).isActive = true
    subview.rightAnchor.constraint(equalTo: rightAnchor, constant: -padding).isActive = true
  }

}

func getTimeFormatedString(_ duration: TimeInterval) -> String {
  let roundedDuration = duration.rounded()
  let formatter = DateComponentsFormatter()
  formatter.zeroFormattingBehavior = .pad

  if roundedDuration >= 3600 {
    formatter.allowedUnits = [.hour, .minute, .second]
  } else {
    formatter.allowedUnits = [.minute, .second]
  }

  return formatter.string(from: roundedDuration) ?? ""
}
