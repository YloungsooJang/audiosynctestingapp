//
//  ViewController.swift
//  AudioSync
//
//  Created by 장영수 on 2020/10/16.
//

import UIKit
import Photos

class ViewController: UIViewController {

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .darkContent
  }

  private let switchView = UIView()
  private let methodSwitch = UISwitch()
  private let stateLabel = UILabel()
  private var collectionView: UICollectionView?

  private var exportSession: CoreExportSession?

  private var viewModels: [ViewModel] = [] {
    didSet {
      self.collectionView?.reloadData()
    }
  }
  private var useAVMutableComposition: Bool = true

  private let columnCount: CGFloat = 3
  private let spacing: CGFloat = 4

  override func viewDidLoad() {
    super.viewDidLoad()

    setupView()
    checkAuthorization()
  }

  private func fetchAssets() {
    var viewModels: [ViewModel] = []
    DispatchQueue.global().async {
      let fetchAssets = PHAsset.fetchAssets(with: .video, options: nil)
      fetchAssets.enumerateObjects({ (phAsset, _, _) in
        viewModels.append(ViewModel(phAsset: phAsset))
      })

      DispatchQueue.main.async {
        self.viewModels = viewModels.reversed()
      }
    }
  }

  private func checkAuthorization() {
    let status = PHPhotoLibrary.authorizationStatus()
    if status == .authorized {
      DispatchQueue.main.async {
        self.fetchAssets()
      }
      return
    }

    if status == .notDetermined {
      PHPhotoLibrary.requestAuthorization { _ in
        self.checkAuthorization()
      }
      return
    }

    DispatchQueue.main.async {
      if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
      }
    }
  }

  private func setupView() {
    view.backgroundColor = UIColor.white
    setSwitchView()
    setStateLabel()
    setCollectionView()
  }

  private func setSwitchView() {
    view.addSubview(switchView)

    switchView.translatesAutoresizingMaskIntoConstraints = false
    switchView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
    switchView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    switchView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

    let titleLabel = UILabel()
    titleLabel.text = "use AVMutableComposition"
    titleLabel.textColor = UIColor.black
    titleLabel.font = UIFont.systemFont(ofSize: 15)
    switchView.addSubview(titleLabel)

    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.topAnchor.constraint(equalTo: switchView.topAnchor, constant: 10).isActive = true
    titleLabel.bottomAnchor.constraint(equalTo: switchView.bottomAnchor, constant: -10).isActive = true
    titleLabel.leftAnchor.constraint(equalTo: switchView.leftAnchor, constant: 10).isActive = true

    methodSwitch.isOn = useAVMutableComposition
    methodSwitch.addTarget(self, action: #selector(onClickSwitch(sender:)), for: .valueChanged)
    switchView.addSubview(methodSwitch)

    methodSwitch.translatesAutoresizingMaskIntoConstraints = false
    methodSwitch.topAnchor.constraint(equalTo: switchView.topAnchor, constant: 10).isActive = true
    methodSwitch.bottomAnchor.constraint(equalTo: switchView.bottomAnchor, constant: -10).isActive = true
    methodSwitch.rightAnchor.constraint(equalTo: switchView.rightAnchor, constant: -10).isActive = true
  }

  @objc func onClickSwitch(sender: UISwitch) {
    self.useAVMutableComposition = sender.isOn
  }

  private func setStateLabel() {
    stateLabel.text = "choose video to export"
    stateLabel.textColor = UIColor.black
    stateLabel.font = UIFont.systemFont(ofSize: 15)
    stateLabel.textAlignment = .center
    stateLabel.numberOfLines = 0
    view.addSubview(stateLabel)

    stateLabel.translatesAutoresizingMaskIntoConstraints = false
    stateLabel.topAnchor.constraint(equalTo: switchView.bottomAnchor, constant: 10).isActive = true
    stateLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    stateLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
  }

  private func setCollectionView() {
    let layout = UICollectionViewFlowLayout()
    layout.minimumInteritemSpacing = self.spacing
    layout.minimumLineSpacing = self.spacing

    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.backgroundColor = nil
    collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    view.addSubview(collectionView)

    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.topAnchor.constraint(equalTo: stateLabel.bottomAnchor, constant: 10).isActive = true
    collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

    collectionView.register(
      CollectionViewCell.self,
      forCellWithReuseIdentifier: CollectionViewCell.identifier
    )
    collectionView.dataSource = self
    collectionView.delegate = self

    self.collectionView = collectionView
  }

}

extension ViewController: UICollectionViewDataSource {

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.viewModels.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: CollectionViewCell.identifier,
      for: indexPath
      ) as? CollectionViewCell

    let viewModel = self.viewModels[indexPath.item]
    cell?.setViewModel(viewModel)

    return cell!
  }

}

extension ViewController: UICollectionViewDelegateFlowLayout {

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
    let width = floor((collectionView.bounds.width - (columnCount - 1) * spacing) / columnCount)
    return CGSize(width: width, height: width)
  }

}

extension ViewController: UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let phAsset = self.viewModels[indexPath.item].phAsset
    let options = PHVideoRequestOptions()
    options.isNetworkAccessAllowed = true
    options.deliveryMode = .highQualityFormat

    stateLabel.text = "exporting..."
    PHImageManager.default().requestAVAsset(forVideo: phAsset, options: options) { (avAsset, _, _) in
      DispatchQueue.main.async {
        if let avAsset = avAsset {
          print(self.useAVMutableComposition)
          self.exportSession = CoreExportSession(
            avAsset: self.useAVMutableComposition ? self.getMutableComposition(avAsset) : avAsset
          )
          self.exportSession?.delegate = self
          self.exportSession?.export()
        }
      }
    }
  }

  private func getMutableComposition(_ asset: AVAsset) -> AVMutableComposition {
    let composition = AVMutableComposition()
    let videoTrack = composition.addMutableTrack(
      withMediaType: AVMediaType.video,
      preferredTrackID: kCMPersistentTrackID_Invalid
    )
    let audioTrack = composition.addMutableTrack(
      withMediaType: AVMediaType.audio,
      preferredTrackID: kCMPersistentTrackID_Invalid
    )

    let assetVideoTrack = asset.tracks(withMediaType: AVMediaType.video).first
    let assetAudioTrack = asset.tracks(withMediaType: AVMediaType.audio).first
    let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
    let atTime = CMTime.zero
    do {
      if let assetVideoTrack = assetVideoTrack {
        try videoTrack?.insertTimeRange(
          timeRange,
          of: assetVideoTrack,
          at: atTime
        )
        videoTrack?.preferredTransform = assetVideoTrack.preferredTransform
      }
      if let assetAudioTrack = assetAudioTrack {
        try audioTrack?.insertTimeRange(
          timeRange,
          of: assetAudioTrack,
          at: atTime
        )
      }
    } catch {
    }

    return composition
  }

}

extension ViewController: CoreExportSessionDelegate {

  func coreExportSessionCompletedHandler() {
    DispatchQueue.main.async {
      self.stateLabel.text = "exporting finished"
      self.fetchAssets()
    }
  }

  func coreExportSessionProgressHandler(progress: Float) {
    DispatchQueue.main.async {
      self.stateLabel.text = "exporting... \(Int(progress * 100))%"
    }
  }

}
