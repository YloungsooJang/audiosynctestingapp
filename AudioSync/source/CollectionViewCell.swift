//
//  CollectionViewCell.swift
//  AudioSync
//
//  Created by 장영수 on 2020/10/16.
//

import UIKit
import Photos

class CollectionViewCell: UICollectionViewCell {

  private var viewModel: ViewModel? {
    didSet {
      if let viewModel = viewModel {
        let phAsset = viewModel.phAsset
        durationLabel.text = getTimeFormatedString(phAsset.duration)

        if let image = viewModel.thumbnail {
          representedAssetIdentifier = ""
          thumbnail.image = image
        } else {
          representedAssetIdentifier = phAsset.localIdentifier
          viewModel.fetchThumbnail(size: frame.size) { image in
            if self.representedAssetIdentifier == phAsset.localIdentifier {
              self.thumbnail.image = image
            }
          }
        }
      }
    }
  }

  private let thumbnail = UIImageView()
  private let overlayView = UIView()
  private let durationLabel = UILabel()

  static let identifier = "CollectionViewCell"
  private var representedAssetIdentifier: String = ""

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupView()
  }

  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // public

  func setViewModel(_ viewModel: ViewModel) {
    self.viewModel = viewModel
  }

  // private

  private func setupView() {
    clipsToBounds = true
    layer.cornerRadius = 3

    setThumbnail()
  }

  private func setThumbnail() {
    thumbnail.contentMode = .scaleAspectFill
    thumbnail.clipsToBounds = true
    addSubviewByFilling(thumbnail)

    setOverlayView()
  }

  private func setOverlayView() {
    overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
    thumbnail.addSubviewByFilling(overlayView)

    setDurationLabel()
  }

  private func setDurationLabel() {
    durationLabel.textColor = UIColor.white
    durationLabel.font = UIFont.systemFont(ofSize: 13)
    durationLabel.textAlignment = .center
    overlayView.addSubview(durationLabel)

    durationLabel.translatesAutoresizingMaskIntoConstraints = false
    durationLabel.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor, constant: -4).isActive = true
    durationLabel.leftAnchor.constraint(equalTo: overlayView.leftAnchor, constant: 16).isActive = true
    durationLabel.rightAnchor.constraint(
      equalTo: overlayView.rightAnchor,
      constant: -16
    ).isActive = true
  }

}
